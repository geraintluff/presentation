var Presentation = (function () {
	function PresentationElement(host) {
		this._stateHandlers = [];
		this.setHost(host);
	}
	PresentationElement.prototype = {
		setHost: function (host) {
			this.host = host;
			this.children = [];
			this._childIds = {};
			this._state = 0;

			if (host) {
				this.scan();
				this._stateBefore();
			}
		},
		state: function (func) {
			if (typeof func === 'function') {
				this._stateHandlers.push(func);
				setTimeout(this._stateChange.bind(this), 0);
				return;
			}
			var child = this.children[this._state];
			var result = child ? child.state() : [];
			if (this._state >= 0) {
				result.unshift(this._state);
			}
			return result;
		},
		child: function (index) {
			return this.children[index];
		},
		current: function () {
			return this._state;
		},
		length: function () {
			return this.children.length;
		},
		seek: function (state) {
			if (typeof state === 'number') {
				state = [state];
			}
			var index = +state.shift();
			if (index < -1 || typeof index !== 'number') {
				index = -1;
			} else if (index >= this.children.length) {
				index = this.children.length - 1;
			}
			this._stateActive(index);
			
			var child = this.children[index];
			if (child) {
				if (state.length) {
					child.seek(state);
				} else {
					child.seek(-1);
				}
			}
			this._stateChange();
		},
		prev: function (direct) {
			if (direct) {
				var index = this._state - 1;
				if (this._state < -1) {
					index = -1;
				} else if (this._state > this.children.length) {
					index = this.children.length - 1;
				}
				if (index >= -1) {
					this._stateActive(index);
					this._stateChange();
					return true;
				} else {
					this._stateBefore();
					this._stateChange();
					return false;
				}
			}
			var child = this.children[this._state];
			if (child && child.prev()) {
				this._stateChange();
				return true;
			} else {
				return this.prev(true);
			}
		},
		next: function (direct) {
			if (direct) {
				var index = this._state + 1;
				if (this._state < -1 || this._state >= this.children.length) {
					index = -1;
				}
				if (index < this.children.length) {
					this._stateActive(index);
					this._stateChange();
					return true;
				} else {
					this._stateAfter();
					this._stateChange();
					return false;
				}
			}
			var child = this.children[this._state];
			if (child && child.next()) {
				this._stateChange();
				return true;
			} else {
				return this.next(true);
			}
		},
		_stateChange: function () {
			for (var i = 0; i < this._stateHandlers.length; i++) {
				var func = this._stateHandlers[i];
				func.call(this, this.state());
			}
		},
		_stateBefore: function () {
			if (this._state > -Infinity) {
				if (this._state >= this.children.length) {
					this._state = this.children.length - 1;
				}
				while (this._state >= 0) {
					var child = this.children[this._state];
					if (child) child._stateBefore();
					this._state--;
				}
				this._state = -Infinity;
				this.uiBefore(this.host);
			}
		},
		_stateActive: function (index) {
			if (index > this.children.length) {
				index = this.children.length - 1;
			}
			if (this._state >= this.children.length) {
				this._state = this.children.length;
				this.uiActive(this.host);
			} else if (this._state < 0) {
				this._state = -1;
				this.uiActive(this.host);
			}
			while (this._state > index) {
				var child = this.children[this._state];
				if (child) {
					child._stateBefore();
				}

				this._state--;

				var child = this.children[this._state];
				if (child) {
					child._stateActive(Infinity);
				}
			}
			while (this._state < index) {
				var child = this.children[this._state];
				if (child) {
					child._stateAfter();
				}

				this._state++;

				var child = this.children[this._state];
				if (child) {
					child._stateActive(-1);
				}
			}
		},
		_stateAfter: function () {
			if (this._state < Infinity) {
				if (this._state < -1) {
					this._state = -1;
				}
				while (this._state < this.children.length) {
					var child = this.children[this._state];
					if (child) child._stateAfter();
					this._state++;
				}
				this._state = Infinity;
				this.uiAfter(this.host);
			}
		},
		uiBefore: function (host) {
			host.className = host.className.replace(/(^|\s)(before|active|after)($|\s)/g, '') + ' before';
		},
		uiActive: function (host) {
			host.className = host.className.replace(/(^|\s)(before|active|after)($|\s)/g, '') + ' active';
		},
		uiAfter: function (host) {
			host.className = host.className.replace(/(^|\s)(before|active|after)($|\s)/g, '') + ' after';
		},
		scan: function () {
			var host = this.host;
			
			var sections = host.getElementsByTagName('section');
			for (var i = 0; i < sections.length; i++) {
				var childNode = sections[i];
				childNode.id = childNode.id || Math.random();
				if (this._childIds[childNode.id]) continue;
				
				var parent = childNode.parentNode;
				while (parent.tagName.toLowerCase() !== 'section' && parent !== host) {
					parent = parent.parentNode;
				}
				if (parent === host) {
					var index = this.children.length;
					var child = this._childIds[childNode.id] = new PresentationElement(childNode);
					this.children.push(child);
					if (index < this._state) {
						child._stateAfter();
					} else if (index === this._state) {
						child._stateActive(-1);
					}
				}
			}
		}
	};
	
	function Presentation(host, options) {
		if (!(this instanceof Presentation)) return new Presentation(host, options);
		PresentationElement.call(this, null);
		var thisPresentation = this;

		this._options = options = options || {};
		options.hash = options.hash || ((!host || host === document.body) && options.hash !== false);

		function updateFromHash() {
			var href = window.location.href;
			var hash = (href.indexOf('#') + 1) ? href.replace(/.*#/, '') : '';
			var state = (hash || '0').split('-').map(parseFloat);
			if (host) thisPresentation.seek(state);
			return state;
		}
		var initialState = updateFromHash();

		if (!host && document.body) {
			thisPresentation.setHost(host = document.body);
			thisPresentation.seek(initialState);
		}
		if (options.hash) {
			updateFromHash();
			window.addEventListener('hashchange', updateFromHash);
			thisPresentation.state(function (state) {
				if (host) {
					window.location.replace('#' + state.join('-'));
				}
			});
		}
		window.addEventListener('load', function () {
			if (!host) {
				thisPresentation.setHost(host = document.body);
			}
			thisPresentation.scan();
			thisPresentation.seek(initialState);
		});
	}
	Presentation.prototype = Object.create(PresentationElement.prototype);
	Presentation.prototype.setHost = function (host) {
		if (host === this.host) return;
		PresentationElement.prototype.setHost.call(this, host);
		if (host) {
			this._stateActive(0);
			if (this._options.keyboard !== false) {
				this.catchKeyboardEvents(host);
			}
		}
	};
	Presentation.prototype.prev = function (big) {
		var result = PresentationElement.prototype.prev.call(this, big);
		// Refuse to navigate before first item
		if (this.current() <= 0) {
			this._stateActive(0);
		}
		return result;
	};
	Presentation.prototype.next = function (big) {
		var result = PresentationElement.prototype.next.call(this, big);
		// Refuse to navigate after last item
		if (this.current() >= this.length() - 1) {
			this._stateActive(this.length() - 1);
		}
		return result;
	};
	Presentation.prototype.catchKeyboardEvents = function (host) {
		var thisPresentation = this;
		host.addEventListener('keydown', function (evt) {
			var code = evt.keyCode || event.which;
			var bigJump = evt.altKey || evt.ctrlKey || evt.metaKey || evt.shiftKey;
			bigJump = bigJump || (code === 33 || code === 34); // PageUp/PageDown
			if (code === 37 || code === 38 || code === 33 || (code === 8 && !bigJump)) { // Left/Up/PageUp or BackSpace without modifiers
				thisPresentation.prev(bigJump) && evt.preventDefault();
			} else if (code === 39 || code === 40 || code === 34 || (code === 32 && !bigJump) || (code === 13 && !bigJump)) { // Right/Down/PageDown, or Space/Enter without modifiers
				thisPresentation.next(bigJump) && evt.preventDefault();
			}
		});
	};
	
	return Presentation;
})();
