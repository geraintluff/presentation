# Minimal presentation library

This is a presentation library that aims for simplicity.

The JS code switches CSS classes on `<section>` elements (`.before`/`.active`/`.after`).  All visibility/slides/reveals/highlights are done using CSS rules, making it very simple to extend with your own visual elements.

```html
<html>
	<head>
		<title>Example presentation</title>
		<link rel="stylesheet" href="presentation.css" />
		<link rel="stylesheet" href="style.css" />
		<script src="presentation.js"></script>
	</head>
	<body>
		<section>
			<h1>Presentation</h1>
			<h3>(press <code>DOWN</code>/<code>ENTER</code> to progress)</h3>
		</section>
		<section>
			<h2>Simple syntax</h2>
			<ul>
				<section><li>Top-level <code>&lt;section&gt;</code> elements are slides</li></section>
				<section><li>Nested <code>&lt;section&gt;</code> elements are progressively revealed within the slides</li></section>
			</ul>
		</section>
		<section>
			<h2>Decoupled code</h2>

			<h3><code>presentation.css</code></h3>
			<p>Minimal CSS (just enough to make the slideshow work) - uses <code>@media</code> queries for print-compatibility (no separate sheet)</p>

			<h3><code>presentation.js</code></h3>
			<p>Switches slides/state using CSS classes: <code>.before</code>/<code>.active</code>/<code>.after</code></p>
		</section>
		<section>
			<h2>Custom style</h2>
			<p>Example in <code>style.css</code></p>
		</section>
	</body>
</html>
```